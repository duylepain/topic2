import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem, MessageService} from 'primeng/api';                  //api
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChipsModule} from 'primeng/chips';
import { PagesComponent } from './pages/pages.component';
import { LayoutColumnComponent } from './@theme/layout/layout-column/layout-column.component';
import { LoginComponent } from './@theme/layout/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './@core/shared/shared.module';
import {ButtonModule} from 'primeng/button';
import { PasswordModule } from 'primeng/password';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { LoginService } from './@core/services/login.service';
import { LayoutColumnRoutingModule } from './@theme/layout/layout-column/layout-column-routing.module';
import { LayoutColumnModule } from './@theme/layout/layout-column/layout-column.module';
import { HeaderComponent } from './@theme/components/header/header.component';
import { SidebarComponent } from './@theme/components/sidebar/sidebar.component';
import { DashboardComponent } from './@theme/components/dashboard/dashboard.component';
import { CoursesComponent } from './@theme/components/courses/courses.component';
import { LessonListComponent } from './@theme/components/lesson-list/lesson-list.component';
import { CourseDetailComponent } from './@theme/components/course-detail/course-detail.component';
import { MyCourseComponent } from './@theme/components/my-course/my-course.component';
import { LayoutWithRightsidebarComponent } from './@theme/layout/layout-with-rightsidebar/layout-with-rightsidebar.component';
import { RightSidebarComponent } from './@theme/components/right-sidebar/right-sidebar.component';
import { ContentLessonComponent } from './@theme/components/content-lesson/content-lesson.component';
import { ComponentsModule } from './@theme/components/components.module';
import { SignInComponent } from './@theme/layout/sign-in/sign-in.component';

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    LoginComponent,
    // HeaderComponent,
    // SidebarComponent,
    // DashboardComponent,
    // CoursesComponent,
    // LessonListComponent,
    // CourseDetailComponent,
    // MyCourseComponent,
    LayoutWithRightsidebarComponent,
    SignInComponent,
    // RightSidebarComponent,
    // ContentLessonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule,
    BrowserAnimationsModule,
    ChipsModule,
    ReactiveFormsModule,
    SharedModule,
    PasswordModule,
    LayoutColumnRoutingModule,
    LayoutColumnModule,
    ComponentsModule
  ],
  providers: [HttpClient, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
