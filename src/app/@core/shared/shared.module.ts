import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonModule} from 'primeng/button';
import {PasswordModule} from 'primeng/password';
import {InputMaskModule} from 'primeng/inputmask';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {ToastModule} from 'primeng/toast';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ProgressBarModule} from 'primeng/progressbar';
import {MenubarModule} from 'primeng/menubar';
import {MenuItem} from 'primeng/api';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {MenuModule} from 'primeng/menu';
import {CardModule} from 'primeng/card';
import {MegaMenuModule} from 'primeng/megamenu';
import {TreeModule} from 'primeng/tree';
import {ContextMenuModule} from 'primeng/contextmenu';
import {PanelMenuModule} from 'primeng/panelmenu';
import {CarouselModule} from 'primeng/carousel';
import {FieldsetModule} from 'primeng/fieldset';
import {PanelModule} from 'primeng/panel';
import {SkeletonModule} from 'primeng/skeleton';
import {RippleModule} from 'primeng/ripple';
import {DragDropModule} from 'primeng/dragdrop';
import {SidebarModule} from 'primeng/sidebar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {DialogModule} from 'primeng/dialog';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {ImageModule} from 'primeng/image';
import {FileUploadModule} from 'primeng/fileupload';
import {RatingModule} from 'primeng/rating';
import {SpeedDialModule} from 'primeng/speeddial';
import {TooltipModule} from 'primeng/tooltip';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {StyleClassModule} from 'primeng/styleclass';
import {RadioButtonModule} from 'primeng/radiobutton';
import {ChartModule} from 'primeng/chart';
import { AppConfigService } from 'src/app/@theme/components/dashboard/appconfigservice';
import {GalleriaModule} from 'primeng/galleria';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {BadgeModule} from 'primeng/badge';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {TimelineModule} from 'primeng/timeline';
import {KeyFilterModule} from 'primeng/keyfilter';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ButtonModule,
    PasswordModule,
    InputMaskModule,
    HttpClientModule,
    ToastModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    MenubarModule,
    AvatarModule,
    AvatarGroupModule,
    MenuModule,
    CardModule,
    MegaMenuModule,
    TreeModule,
    ContextMenuModule,
    PanelMenuModule,
    CarouselModule,
    FieldsetModule,
    PanelModule,
    SkeletonModule,
    RippleModule,
    DragDropModule,
    SidebarModule,
    FontAwesomeModule,
    DialogModule,
    DynamicDialogModule,
    ReactiveFormsModule,
    InputTextModule,
    ImageModule,
    FileUploadModule,
    RatingModule,
    FormsModule,
    SpeedDialModule,
    TooltipModule,
    ConfirmDialogModule,
    StyleClassModule,
    RadioButtonModule,
    ChartModule,
    GalleriaModule,
    DropdownModule,
    InputTextareaModule,
    BadgeModule,
    OverlayPanelModule,
    TimelineModule,
    KeyFilterModule,

  ],
  exports: [
    ButtonModule,
    PasswordModule,
    InputMaskModule,
    HttpClientModule,
    ToastModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    MenubarModule,
    AvatarModule,
    AvatarGroupModule,
    MenuModule,
    CardModule,
    MegaMenuModule,
    TreeModule,
    ContextMenuModule,
    PanelMenuModule,
    CarouselModule,
    FieldsetModule,
    PanelModule,
    SkeletonModule,
    RippleModule,
    DragDropModule,
    SidebarModule,
    FontAwesomeModule,
    DialogModule,
    DynamicDialogModule,
    ReactiveFormsModule,
    InputTextModule,
    ImageModule,
    FileUploadModule,
    RatingModule,
    FormsModule,
    SpeedDialModule,
    TooltipModule,
    ConfirmDialogModule,
    StyleClassModule,
    RadioButtonModule,
    ChartModule,
    GalleriaModule,
    DropdownModule,
    InputTextareaModule,
    BadgeModule,
    OverlayPanelModule,
    TimelineModule,
    KeyFilterModule

  ],
  providers: [HttpClient, DialogService, ConfirmationService, AppConfigService],
})
export class SharedModule { }
