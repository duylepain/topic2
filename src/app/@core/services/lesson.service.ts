import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { baseUrl } from '../const/baseUrl';

@Injectable({
  providedIn: 'root'
})
export class LessonService {
  url:string = baseUrl;
  constructor(private http: HttpClient) { }

  getLesson(id:number):Observable<any>{
    return this.http.get<any>(this.url + '/lesson/' + id).pipe(map((res:any) => res));
  }
}
