import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from '../const/baseUrl';

@Injectable({
  providedIn: 'root'
})
export class ContentLessonService {

  // variable
  idLesson!:number

  url:string = baseUrl;

  // fucntion
  constructor(private http: HttpClient) {}

  getContentLesson(idCourse:any, id:any):Observable<any>{
    if (id == undefined || id == null || id == ''){
      return this.http.get<any>(this.url + '/'+ idCourse + '/1')
    }else{
      return this.http.get<any>(this.url + '/'+ idCourse + '/' + id)
    }
  }
}
