import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { baseUrl } from '../const/baseUrl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = baseUrl;
  constructor(private http: HttpClient, private router: Router) {}

  // header = new Headers().set('Authorization', `Bearer ${this.oauthService.getAccessToken()}`)

  getAccount():Observable<any>{
    return this.http.get(this.url+'/account')
    .pipe(map((res:any) => res));
  }
  registerAccout(data:any):Observable<any>{
    return this.http.post(this.url+'/account', data).pipe(map((res:any)=>res));
  }

  isLoggednIn():boolean{
    if(sessionStorage.getItem('isLogin')){
      return true;
    }else{
      return false;
    }
  }
  logOut(){
    sessionStorage.removeItem('isLogin');
    this.router.navigate(['login']);

  }
}
