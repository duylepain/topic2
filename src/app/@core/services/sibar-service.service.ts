import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { baseUrl } from '../const/baseUrl';

@Injectable({
  providedIn: 'root'
})
export class SibarServiceService {
  url = baseUrl+'/sidebarTree';
  constructor(private http: HttpClient) {}

  getNodeTree():Observable<any> {
    return this.http.get<any>(this.url)
    .pipe(map((res:any) => res));
  }
}
