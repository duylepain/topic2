import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutColumnComponent } from '../@theme/layout/layout-column/layout-column.component';


@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  constructor() { }
  display = false;

  items = [{
    label: 'Menu',
    items: [
        {label: 'Dashboard', icon: 'pi pi-chart-pie', routerLink: 'dashboard'},
        {label: 'Khoá học', icon: 'pi pi-book'},
        {label: 'Cài đặt', icon: 'pi pi-sliders-h'}
    ]
},
// {
//   icon: 'pi pi-repeat',
//     label: 'Edit',
//     items: [
//         {label: 'Undo', icon: 'pi pi-refresh'},
//         {label: 'Redo', icon: 'pi pi-repeat'}
//     ]
// }
];
  ngOnInit(): void {
  }
  showSidebar(){
    this.display = true
  }
}
