import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseDetailComponent } from '../@theme/components/course-detail/course-detail.component';
import { CoursesComponent } from '../@theme/components/courses/courses.component';
import { DashboardComponent } from '../@theme/components/dashboard/dashboard.component';
import { MyCourseComponent } from '../@theme/components/my-course/my-course.component';
import { PagesComponent } from './pages.component';
import { LayoutWithRightsidebarComponent } from '../@theme/layout/layout-with-rightsidebar/layout-with-rightsidebar.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'course', pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'course',
        component: CoursesComponent,
      },
      {
        path: 'mycourses',
        loadChildren: () => import('./../@theme/components/my-course/my-course.module').then(m => m.MyCourseModule)
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
