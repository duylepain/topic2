import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../@core/shared/shared.module';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../@core/services/login.service';
import { LoginComponent } from '../@theme/layout/login/login.component';
import { LayoutColumnComponent } from '../@theme/layout/layout-column/layout-column.component';
import { LayoutColumnModule } from '../@theme/layout/layout-column/layout-column.module';
import { LayoutColumnRoutingModule } from '../@theme/layout/layout-column/layout-column-routing.module';
import { SidebarComponent } from '../@theme/components/sidebar/sidebar.component';


@NgModule({
  declarations: [
    LayoutColumnComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    LayoutColumnModule,
    LayoutColumnRoutingModule,
  ],
  providers: [],
})
export class PagesModule { }
