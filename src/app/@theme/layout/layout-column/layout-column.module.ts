import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutColumnRoutingModule } from './layout-column-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutColumnRoutingModule,
  ]
})
export class LayoutColumnModule { }
