import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LoginService } from 'src/app/@core/services/login.service';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  constructor(private login: LoginService, private messageService: MessageService, private _router: Router) { }
  // variable ==========================
  toggleMask = true;
  checkAcc: boolean = true;
  checkTempAcc!: boolean;
  currentAccount:any = [];
  listAccount = []
  loginAccount = [];
  blockSpecial: RegExp = /^[^<>*!]+$/;
  blockSpace: RegExp = /[^\s]/;
  ccRegex: RegExp = /[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$/;
  cc!: string;

  signinForm = new FormGroup({
    username : new FormControl(),
    firstName : new FormControl(),
    lastName : new FormControl(),
    password : new FormControl(),
    role: new FormControl('user'),
    id_token: new FormControl('jwt demo')

  })

  // function =================================================================
  ngOnInit() {
    // this.login.getAccount().subscribe((data:any)=>{
    //   this.listAccount = data;
    //   console.log(this.listAccount);
    // }
    // )
  }


  onClickSubmit(data:any){
    // this.currentAccount.length = 0;
    // this.currentAccount.push(data.value)
    // // console.log(this.currentAccount)

    // this.listAccount.forEach((acc:any)=>{
    //   if(acc.username == this.currentAccount[0].username && acc.password == this.currentAccount[0].password){
    //     console.log("Success");
    //     sessionStorage.setItem('isLogin', this.currentAccount[0].id_token);
    //     // console.log(sessionStorage.getItem('isLogin'));

    //     setTimeout(()=>{
    //       this._router.navigate(['/pages']);
    //     },5000);
    //     this.checkAcc = false;
    //   }
    // })
    // if(this.checkAcc){
    //   this.messageService.add({key: 'tc', severity:'warn', summary: '', detail: 'Tài khoản không đúng! Vui lòng nhập lại tài khoản hoặc mật khẩu để tiếp tục'});
    // }
    this.login.registerAccout(data.value).subscribe();
  }


  onReject() {
    this.messageService.clear('c');
  }
  onConfirm() {
    this.messageService.clear('c');
  }
  showURL(){
    // console.log(this._router.url);
    this._router.navigate(["../sign-in"])

  }

}
