import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-with-rightsidebar',
  templateUrl: './layout-with-rightsidebar.component.html',
  styleUrls: ['./layout-with-rightsidebar.component.css']
})
export class LayoutWithRightsidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  back(){
    window.history.back();
  }
}
