import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ContentLessonComponent } from '../../components/content-lesson/content-lesson.component';
import { CourseDetailComponent } from '../../components/course-detail/course-detail.component';
import { RefundComponent } from '../../components/refund/refund.component';
import { LayoutWithRightsidebarComponent } from './layout-with-rightsidebar.component';

const route: Routes = [
  {
    path: '',
    component: LayoutWithRightsidebarComponent,
    // children: [
    //   {
    //     path: 'course-detail/:id',
    //     component: RefundComponent
    //   }
    // ]

  },

]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class LayoutWithRightsidebarModule { }
