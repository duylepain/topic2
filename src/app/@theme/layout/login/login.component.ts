import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LoginService } from 'src/app/@core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  blockSpecial: RegExp = /^[^<>*!]+$/;
  blockSpace: RegExp = /[^\s]/;
  ccRegex: RegExp = /[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$/;
  cc!: string;

  constructor(private login: LoginService, private messageService: MessageService, private _router: Router) { }
  // variable ==========================
  toggleMask = true;
  checkAcc: boolean = true;
  checkTempAcc!: boolean;
  currentAccount:any = [];
  listAccount = []
  loginAccount = [];
  loginForm = new FormGroup({
    username : new FormControl(),
    password : new FormControl()

  })

  // function =================================================================
  ngOnInit() {
    this.login.getAccount().subscribe((data:any)=>{
      this.listAccount = data;
      console.log(this.listAccount);
    }
    )
  }
  onClickSubmit(data:any){
    this.currentAccount.length = 0;
    this.currentAccount.push(data.value)
    // console.log(this.currentAccount)

    this.listAccount.forEach((acc:any)=>{
      if(acc.username == this.currentAccount[0].username && acc.password == this.currentAccount[0].password){
        console.log("Success");
        sessionStorage.setItem('isLogin', this.currentAccount[0].id_token);
        // console.log(sessionStorage.getItem('isLogin'));

        setTimeout(()=>{
          this._router.navigate(['/pages']);
        },5000);
        this.checkAcc = false;
      }
    })
    if(this.checkAcc){
      this.messageService.add({key: 'tc', severity:'warn', summary: '', detail: 'Tài khoản không đúng! Vui lòng nhập lại tài khoản hoặc mật khẩu để tiếp tục'});
    }
  }
  onReject() {
    this.messageService.clear('c');
  }
  onConfirm() {
    this.messageService.clear('c');
  }
  showURL(){
    // console.log(this._router.url);
    this._router.navigate(["../sign-in"])

  }
}
