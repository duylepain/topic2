import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoginService } from 'src/app/@core/services/login.service';
import { UserProfileComponent } from '../user-profile/user-profile.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private dialogService: DialogService, private loginSer: LoginService) { }
  items: MenuItem[] =[
    {
      label: 'Thông tin cá nhân',
      icon: 'pi pi-fw pi-user-edit',
      command: () => {
        this.dialogService.open(UserProfileComponent,{header: 'Thông tin cá nhân', width: '30%', contentStyle: {
          height: 'max-content',
          overflow: 'auto',
          'border-radius': '0 0 6px 6px',
          'overflow-y': 'scroll',
        },})}},
    {
      label: 'Đổi mật khẩu',
      icon: 'pi pi-fw pi-lock'},
    {
      label: 'Đăng xuất',
      icon: 'pi pi-fw pi-exclamation-circle',
      command: ()=>{
        this.loginSer.logOut();
      },
    }
  ];

  ngOnInit(): void {
  }
  show(e:any){
    console.log(e);
  }
}
