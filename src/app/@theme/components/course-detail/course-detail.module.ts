import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CourseDetailComponent } from './course-detail.component';
import { LayoutWithRightsidebarComponent } from '../../layout/layout-with-rightsidebar/layout-with-rightsidebar.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutWithRightsidebarComponent,
  },
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CourseDetailModule { }
