import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../courses/course.service';
import { myCourse, Product } from '../courses/courses';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {

  constructor(
    private _activeRoute: ActivatedRoute,
    private _router: Router,
    private _courseService: CourseService
  ) { }

  // variable
  id!: any;
  sub:any;
  product!:myCourse;

  ngOnInit(): void {
    this.sub = this._activeRoute.paramMap.subscribe(para => {
      // console.log(para);
      this.id = para.get('id');
      // let products = this._courseService.getMyCourse();
      // this.product = products.find(p => p.id === this.id);
      this._courseService.getMyCourse(this.id).subscribe(data =>{
        console.log(data);
        this.product = data;
      });
    })
  }
  drag(){
    console.log('OK')
  }
}
