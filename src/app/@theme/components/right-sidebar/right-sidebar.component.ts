import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { CourseDetailService } from 'src/app/@core/services/course-detail.service';
import { LessonService } from 'src/app/@core/services/lesson.service';
import { CourseService } from '../courses/course.service';

@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.css']
})
export class RightSidebarComponent implements OnInit {

  // variable
  lessonList:any;
  selectedFile:any;
  numberLessonTemp: any;
  activeLesson!:boolean;
  idCourse:any;
  idLesson:any;

  //function
  constructor(
    private _courseService: CourseService,
    private messageService: MessageService,
    private _lessonService: LessonService,
    private _courseDetail: CourseDetailService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if(this._courseDetail.idCourse){
      this.idCourse = this._courseDetail.idCourse;
    }else{
      this.idCourse = sessionStorage.getItem('idLesson');
    }
    this._lessonService.getLesson(this.idCourse).subscribe(data => {
      this.lessonList = data.data;
      this.lessonList[0].isActive = true;
      console.log(data.data);
    });
  }

  nodeSelect(event:any) {
    this.messageService.add({severity: 'info', summary: 'Node Selected', detail: event.node.label});
    this.numberLessonTemp = event.node.numberLesson
  }
  nodeUnselect(event:any) {
    this.messageService.add({severity: 'info', summary: 'Node Unselected', detail: event.node.label});
  }
  getIdLesson(e:any){
    console.log(e);
    this.idLesson = e;
    // this.router.navigate(["../course-detail/detail"])
    // console.log(this.router.url);

    // console.log(this.router.url);

  }
}
