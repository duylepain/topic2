import { Component, OnInit } from '@angular/core';


interface OptionBuy {
  name: string,
  code: string
}

@Component({
  selector: 'app-buy-course',
  templateUrl: './buy-course.component.html',
  styleUrls: ['./buy-course.component.scss']
})
export class BuyCourseComponent implements OnInit {
  optionBuy!:OptionBuy[]
  selectedOption:any;
    constructor() { }


    ngOnInit() {
      this.optionBuy = [
        {name: 'Ví điện tử MOMO', code: 'MM'},
        {name: 'Thẻ ATM', code: 'ATM'},
        {name: 'Ví yudev nội bộ', code: 'YUDEV'},
    ];
    }

    logOptionBuy(e:any){
      this.selectedOption = e.code
      console.log(e);
      console.log(this.selectedOption)
    }
    onBasicUpload(e:any){

    }
}
