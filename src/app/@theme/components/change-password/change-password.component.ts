import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {



  changePassword = new FormGroup({
    newPassword: new FormControl(''),
    confirmNewPass: new FormControl('')
  })

  constructor() { }

  ngOnInit(): void {
  }

  onClickSubmit(form:any){
    console.log(form);
  }
  editUser(){

  }

}
