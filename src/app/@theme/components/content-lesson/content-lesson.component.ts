import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import {
  ConfirmationService,
  ConfirmEventType,
  MessageService,
} from 'primeng/api';
import { ContentLessonService } from 'src/app/@core/services/content-lesson.service';
import { CourseDetailService } from 'src/app/@core/services/course-detail.service';

@Component({
  selector: 'app-content-lesson',
  templateUrl: './content-lesson.component.html',
  styleUrls: ['./content-lesson.component.css'],
})
export class ContentLessonComponent implements OnInit {
  @Input() numberLesson: any;
  // numberLesson:any;
  // variable ==========================================
  title: string = 'Bài 1';
  lessonSelected: any;
  contentLesson: any;
  loading = [false, false, false, false];
  unlockTest: boolean = false;
  city!: string;
  selectedCategory: any = null;

  listQuestion: any[] = [
    { name: 'Accounting', key: 'A' },
    { name: 'Marketing', key: 'M' },
    { name: 'Production', key: 'P' },
    { name: 'Research', key: 'R' },
  ];

  // function =============================================
  constructor(
    private _contentLessonService: ContentLessonService,
    private _courseDetail: CourseDetailService,
    private _confirmationService: ConfirmationService,
    private _messageService: MessageService
  ) {}

  ngOnChanges(change: SimpleChange): void {
    // console.log(this.numberLesson);
    if (!this._courseDetail.idCourse) {
      this._courseDetail.idCourse = sessionStorage.getItem('idCourse');
    }
    this._contentLessonService
      .getContentLesson(this._courseDetail.idCourse, this.numberLesson)
      .subscribe((res) => {
        console.log(res);
        this.contentLesson = res;
        this.listQuestion = res.answers;
      });
    console.log(this.numberLesson);
  }
  ngOnInit(): void {
    this.selectedCategory = this.listQuestion[1];
  }

  load(index: any) {
    this.loading[index] = true;
    setTimeout(() => (this.loading[index] = false), 1000);
  }

  show(e: any) {
    // console.log(e);
  }
  confirm1() {
    this._confirmationService.confirm({
      header: 'Xác nhận hoàn thành bài học',
      message: 'Đồng ý để mở khóa bài test đánh giá',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this._messageService.add({
          severity: 'success',
          summary: 'Successful',
          detail: 'Mở khóa bài test',
        });
        this.unlockTest = true;
      },
      reject: (type: any) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this._messageService.add({
              severity: 'error',
              summary: 'Rejected',
              detail: 'You have rejected',
            });
            break;
          case ConfirmEventType.CANCEL:
            this._messageService.add({
              severity: 'warn',
              summary: 'Cancelled',
              detail: 'You have cancelled',
            });
            break;
        }
      },
    });
  }

  submit() {
    console.log('ok');
    this._messageService.clear();
    this._messageService.add({
      key: 'c',
      sticky: true,
      severity: 'success',
      summary: 'Xác nhận nộp?',
      detail: 'Confirm to proceed',
    });
  }
  onConfirm() {
    this._messageService.clear('c');
    if(true){
      this._messageService.add({ severity: 'success', summary: 'Successful', detail: 'Mở khóa bài test', });
    }else{
      this._messageService.add({severity: 'error', summary: 'Rejected', detail: 'You have rejected', });
    }
  }

  onReject() {
    this._messageService.clear('c');
  }
}
