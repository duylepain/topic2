import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses/courses.component';
import { LessonListComponent } from './lesson-list/lesson-list.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentLessonComponent } from './content-lesson/content-lesson.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { MyCourseComponent } from './my-course/my-course.component';
import { SharedModule } from 'src/app/@core/shared/shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { BuyCourseComponent } from './buy-course/buy-course.component';
import { RefundComponent } from './refund/refund.component';
import { RoadmapComponent } from './roadmap/roadmap.component';
import { ChangePasswordComponent } from './change-password/change-password.component';



@NgModule({
  declarations: [
    ContentLessonComponent,
    CourseDetailComponent,
    CoursesComponent,
    DashboardComponent,
    HeaderComponent,
    LessonListComponent,
    MyCourseComponent,
    RightSidebarComponent,
    SidebarComponent,
    UserProfileComponent,
    BuyCourseComponent,
    RefundComponent,
    RoadmapComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    ContentLessonComponent,
    CourseDetailComponent,
    CoursesComponent,
    DashboardComponent,
    HeaderComponent,
    LessonListComponent,
    MyCourseComponent,
    RightSidebarComponent,
    SidebarComponent,
    UserProfileComponent,
    BuyCourseComponent,
    RefundComponent,
    RoadmapComponent,
  ]
})
export class ComponentsModule { }
