import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss']
})
export class RefundComponent implements OnInit {

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
  }


  sendRequest(){
    this.messageService.add({key: 'tc', severity:'success', summary:'', detail:'Gửi yêu cầu hoàn tiền thành công !', life: 1000})
  }
}
