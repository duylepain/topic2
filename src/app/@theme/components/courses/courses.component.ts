import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BuyCourseComponent } from '../buy-course/buy-course.component';
import { MyCourseComponent } from '../my-course/my-course.component';
import { RoadmapComponent } from '../roadmap/roadmap.component';
import { CourseService } from './course.service';
import { myCourse, Product } from './courses';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
  constructor(
    private coursesSer: CourseService,
    private _router: Router,
    private messageService: MessageService,
    private dialogService: DialogService
  ) {}
  // variable ===============================================
  courses: Product[] = [];
  responsiveOptions = [];
  myCourse: myCourse[] = [];
  items!: MenuItem[];
  ref!: DynamicDialogRef;
  //ngOnInit() ==============================================
  ngOnInit(): void {
    this.coursesSer.getCourse().subscribe((data) => {
      console.log(data);
      this.courses = data;
    });
    this.coursesSer.getMyCourse().subscribe((data) => {
      this.myCourse = data;
      // data.forEach((cour:any)=>{
      //   this.myCourse.push(cour);
      // })
      console.log(data);
    });
  }

  showEvent(e: any) {
    this._router.navigate(['id']);
  }
  show() {
    this.ref = this.dialogService.open(BuyCourseComponent, {
      data: {
        course: this.courses,
      },
      header: 'Mua khóa học',
      width: '50%',
      contentStyle: {
        'max-height': '50%',
        height: '500px',
        overflow: 'auto',
        'border-radius': '0 0 5px 5px',
        'overflow-y': 'scroll',
      },
      baseZIndex: 10000,
    });

    this.ref.onClose.subscribe((product: Product) => {
      if (product) {
        this.messageService.add({
          severity: 'info',
          summary: 'Product Selected',
          detail: product.name,
        });
      }
    });
  }
  showRoadmap() {
    this.ref = this.dialogService.open(RoadmapComponent, {
      data: {
        course: this.courses,
      },
      header: 'Mua khóa học',
      width: '50%',
      contentStyle: {
        'max-height': '50%',
        height: '500px',
        overflow: 'auto',
        'border-radius': '0 0 5px 5px',
        'overflow-y': 'scroll',
      },
      baseZIndex: 10000,
    });

    this.ref.onClose.subscribe((product: Product) => {
      if (product) {
        this.messageService.add({
          severity: 'info',
          summary: 'Product Selected',
          detail: product.name,
        });
      }
    });
  }
}
