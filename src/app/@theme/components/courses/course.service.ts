import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { baseUrl } from '../../../@core/const/baseUrl';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor( private http: HttpClient) { }
  url = baseUrl;
  getCourse():Observable<any>{
    return this.http.get<any>(this.url+ '/courses')
    .pipe(map((res:any) => res));
  }
  getMyCourse(id?: string):Observable<any>{
    let urlTemp = this.url+ '/myCourse';
    if(id !== '' && id !== undefined && id !== null){
      urlTemp = urlTemp.concat('/'+ id);
    }
    return this.http.get<any>(urlTemp)
    .pipe(map((res:any) => res));
  }
  
}
