import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, TreeNode, MessageService } from 'primeng/api';
import { baseUrl } from 'src/app/@core/const/baseUrl';
import { SibarServiceService } from 'src/app/@core/services/sibar-service.service';
import { sidebarAdmin } from './sidebar';
import { topicUrl } from '../../../@core/const/baseUrl';
import { DashboardComponent } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  // variable =============================================================
  routerLink!:string;
  files1 = sidebarAdmin;
  selectedFile:any;
  sidebarMenu: any;

  constructor( private messageService: MessageService, private sidebarSer: SibarServiceService, private _router: Router) {}
  items = [{
    label: 'Menu',
    items: [
        {label: 'Dashboard', icon: 'pi pi-chart-pie', routerLink: ['dashboard'], routerLinkActiveOptions: { exact: true },
        items: [
          {label: 'Undo', icon: 'pi pi-refresh'},
          {label: 'Redo', icon: 'pi pi-repeat'}
        ]
        },
        {label: 'Khoá học', icon: 'pi pi-book', routerLink: ['course'],},
        {label: 'Khoá học của tôi', icon: 'pi pi-book', routerLink: ['mycourses'],},
        {label: 'Cài đặt', icon: 'pi pi-sliders-h', routerLinkActiveOptions: { exact: true }}
    ]
},
];
  ngOnChanges(): void {
  }

  ngOnInit(): void {
    this.sidebarMenu = [
      {
        label: 'Dashboard',
        icon: '<i class="fa-solid fa-chart-line"></i>',
        linkActive: 'dashboard',
      },
      {
        label: 'Home',
        icon: '<i class="fa-brands fa-algolia"></i>',
        linkActive: 'demo',
      },
      {
        label: 'Khóa học',
        icon: '<i class="fa-solid fa-anchor"></i>',
        linkActive: 'course',
      },
      {
        label: 'Khóa học của tôi',
        icon: '<i class="fa-solid fa-book"></i>',
        linkActive: 'mycourses',
      },
      {
        label: 'Thông tin',
        icon: '<i class="fa-brands fa-app-store-ios"></i>',
        linkActive: 'home',
      }
    ]

  }
  nodeSelect(event:any) {
    this.messageService.add({severity: 'info', summary: 'Node Selected', detail: event.node.label});
    // console.log(topicUrl +'/pages/' + event.node.data);
    // this._router.navigate(['/dashboard']);
    // this._router.navigate([event.node.data]);
    this.routerLink = event.node.data;
    console.log(this.routerLink);
    this._router.navigate([this.routerLink]);

  }

  nodeUnselect(event:any) {
    // this.messageService.add({severity: 'info', summary: 'Node Unselected', detail: event.node.label});
  }

}
