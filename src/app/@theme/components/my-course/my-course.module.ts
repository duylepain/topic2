import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CourseDetailComponent } from '../course-detail/course-detail.component';
import { LayoutWithRightsidebarComponent } from '../../layout/layout-with-rightsidebar/layout-with-rightsidebar.component';
import { MyCourseComponent } from './my-course.component';

const routes: Routes = [
  {
    path: '',
    component: MyCourseComponent,
  },
  // {
  //   path: 'course-detail',
  //   loadChildren: () => import('./../course-detail/course-detail.module')
  //     .then(m => m.CourseDetailModule),
  // }
  {
    path: 'course-detail',
    loadChildren: () => import('./../../layout/layout-with-rightsidebar/layout-with-rightsidebar.module')
      .then(m => m.LayoutWithRightsidebarModule),
  },
  // {
  //   path: '',
  //   redirectTo:'course-detail/:id', pathMatch: 'full'
  // }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class MyCourseModule { }
