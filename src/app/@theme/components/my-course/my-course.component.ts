import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { CourseDetailService } from 'src/app/@core/services/course-detail.service';
import { CourseService } from '../courses/course.service';
import { myCourse, Product } from '../courses/courses';
import { RefundComponent } from '../refund/refund.component';

@Component({
  selector: 'app-my-course',
  templateUrl: './my-course.component.html',
  styleUrls: ['./my-course.component.css']
})
export class MyCourseComponent implements OnInit {
  constructor(
    private coursesSer: CourseService,
    private _router: Router,
    private _courseDetail: CourseDetailService,
    private messageService: MessageService,
    private dialogSer: DialogService,
    ) { }
  // variable ===============================================
  courses: Product[] = [];
  responsiveOptions = [];
  myCourse:myCourse[] = [];
  value1: number = 3;
  msg: string= '';
  idLesson!:number;


  //ngOnInit() ==============================================
  ngOnInit(): void {
    this.coursesSer.getCourse().subscribe(data => {
      this.courses = data;
    });
    this.coursesSer.getMyCourse().subscribe(data => {
      this.myCourse = data;
    })
  }

  showEvent(e:any){
    // console.log(e);
    this.idLesson = e;
    this._courseDetail.setIdLesson(e);
    // console.log(this._courseDetail.idLesson)
  }
  refund(e:any){
    this.dialogSer.open(RefundComponent, {
      header: 'Hoàn tiền khóa học',
      width: '30%',
      contentStyle: {'border-radius':'0 0 5px 5px'}

  });
  }
}
