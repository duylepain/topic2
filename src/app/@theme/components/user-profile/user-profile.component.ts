import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  // variable
  disable:boolean = true;
  userProfile = new FormGroup({
    username: new FormControl(),
    displayName: new FormControl(),
    phone: new FormControl(),
  })

  ref!: DynamicDialogRef;

  constructor(private _messageService: MessageService) { }

  ngOnInit(): void {
  }
  onClickSubmit(){

  }
  editUser(){
    this.disable = false;
  }
  onBasicUpload(e:any){
  }

  updateUser(){
    (this.disable) ? this.disable = false : this.disable = true;
    this._messageService.add({key: 'tc', severity:'success', summary: 'Thành công', detail: 'Cập nhật thông tin thành công'});
  }
  onConfirm() {
    this._messageService.clear('c');
  }
  onReject() {
    this._messageService.clear('c');
  }

  clear() {
    this._messageService.clear();
  }
  cancelUpdate(){
    (this.disable)?this.disable = false: this.disable = true;
    this._messageService.add({key: 'tc', severity:'warn', summary: '', detail: 'Hủy cập nhật !'});
  }
}
